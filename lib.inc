section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, rdi
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .counter:
    cmp byte[rdi + rax], 0
    je .end
    inc rax
    jmp .counter
    .end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi
    mov rdi, 1
    mov rdx, rax
    mov rax, 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    xor rdx, rdx
    mov rsi, rsp
    dec rsi
    mov rcx, 10
    push 0
    sub rsp, 16
    test rax, rax
    jz .print_uint_zero
    .print_uint_loop:
    test rax, rax
    jz .print_uint_end
    div rcx
    add rdx, '0'
    dec rsi
    mov [rsi], dl
    xor rdx, rdx
    jmp .print_uint_loop
    .print_uint_zero:
    add rdx, '0'
    dec rsi
    mov [rsi], dl
    .print_uint_end:
    mov rdi, rsi
    call print_string
    add rsp, 24
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rax, rdi
    test rax, rax
    jns .print_int_positive
    .print_minus_char:
    mov rdi, '-'
    push rax
    call print_char
    pop rax
    neg rax
    .print_int_positive:
    mov rdi, rax
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    mov rdi, rdi
    push rdi
    push rsi
    call string_length
    pop rsi
    pop rdi
    mov rcx, rax
    push rdi
    mov rdi, rsi
    xor rax, rax
    push rcx
    call string_length
    pop rcx
    pop rdi
    cmp rax, rcx
    jne .string_equals_not
    cmp rax, rcx
    je .string_equals_yes
    dec rcx
    .string_equals_loop:
    cmp rcx, -1
    jz .string_equals_yes
    mov dl, byte[rdi + rcx]
    mov al, byte[rsi + rcx]
    cmp dl, al
    jne .string_equals_not
    dec rcx
    jmp .string_equals_loop
    .string_equals_not:
    xor rax, rax
    ret
    .string_equals_yes:
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
   push 0
   xor rax, rax
   xor rdi, rdi
   mov rsi, rsp
   mov rdx, 1
   syscall
   pop rax
   ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax
    xor rdx, rdx
    push rdi

    .read_word_skip_at_start:
    push rdx
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    pop rdx
    cmp al, 0x20
    je .read_word_skip_at_start
    cmp al, 0x9
    je .read_word_skip_at_start
    cmp al, 0xA
    je .read_word_skip_at_start

    .read_word_loop:
    cmp rdx, rsi
    je .read_word_longer_than_buffer
    cmp al, 0x20
    je .read_word_end
    cmp al, 0x9
    je .read_word_end
    cmp al, 0xA
    je .read_word_end
    cmp al, 0x0
    je .read_word_end
    mov byte[rdi], al
    inc rdi
    inc rdx
    push rdx
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    pop rdx
    jmp .read_word_loop

    .read_word_end:
    mov byte[rdi], 0
    pop rax
    ret
    .read_word_longer_than_buffer:
    pop rax
    xor rax, rax
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	xor rdx, rdx
	cmp byte [rdi], 0
	je .parse_int_exit
.parse_int_loop:
	movzx rcx, byte [rdi]
	cmp rcx, '0'
	jb .parse_int_exit
	cmp rcx, '9'
	ja .parse_int_exit

	imul rax, rax, 10
	sub rcx, '0'
	add rax, rcx

	inc rdi
	inc rdx

	jmp .parse_int_loop
.parse_int_exit:
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    cmp byte [rdi], '-'
    jne .parse_int_positive
    .parse_int_negative:
    inc rdi
    call parse_uint
    test rax, rax
    je .parse_int_end
    neg rax
    inc rdx
    ret
    .parse_int_positive:
    jmp parse_uint
    .parse_int_end:
    xor rdx, rdx
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx
    .string_copy_loop:
    cmp byte[rdi + rcx], 0
    je .string_copy_end
    mov al, byte[rdi + rcx]
    mov byte[rsi + rcx], al
    inc rcx
    cmp rdx, rcx
    jl .string_copy_wrong_end
    xor rax, rax
    jmp .string_copy_loop
    .string_copy_end:
    cmp rcx, rdx
    jz .string_copy_last_end
    mov byte[rsi + rcx], 0
    .string_copy_last_end:
    mov rax, rcx
    ret
    .string_copy_wrong_end:
    xor rax, rax
    ret
